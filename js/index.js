        //Burger changer
$('.header__burger').click(function(){
    $('.header__burger').toggleClass('cross');
    $('.menu').toggleClass('hide');
});
        //phone mask
$(function () {
  $('.input--tel').mask('+7(999)999-99-99');

  $('.input--tel').on('focus', function () {
     if ($(this).val().length === 0) {
       $(this).val('+7(');
     }
  });
  $('.input--tel').on('focusout', function () {
      if ($(this).val().length <= 4) {
       $(this).val('');
     }
  });

  $('.input--tel').keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
        }

        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
        //Diagnostic changer
$('.diagnostic__ul li').click(function(){
    var numberElem = $(this).index();
    var diaText = '';
    if ($(this).hasClass('active')) {
        return false;
    } else {
        $('.diagnostic__ul .active').removeClass('active');
        $(this).addClass('active');
        switch(numberElem) {
            case 0:
                diaText = 'Комплексная диагностика двигателя, устранение течей, капитальный ремонт, ремонт навесного обрудования, обслуживание ГРМ.';
                break;
            case 1:
                diaText = 'Ремонт АКПП и коробок DSG. Замена рабочих жидкостей, фильтров. Диагностика,ремонт и замена сцеплений DSG, ремонт мехатроников. Ремонт приводов и карданных валов.';
                break;
            case 2:
                diaText = 'Диагностика подвески.Замена рычагов, сайлент-блоков, шаровых соединений. Диагностика, профилактика и ремонт  пневмоподвески. Ремонт рулевых реек. Регулировка углов установки колёс';
                break;
            case 3:
                diaText = 'Обслуживание тормозной системы, замена тормозных дисков и колодок, тормозных шлангов';
                break;
            case 4:
                diaText = 'Диагностика и ремонт электронных систем. Активация скрытых функций, тонкая настройка автомобиля. Кодирование блоков управления, обновление  версий ПО, калибровка и  адаптация исполнительных механизмов,  установка базовых величин.';
                break;
            case 5:
                diaText = 'Шиномонтажные и балансировка. Контроль геометрии дисков,контроль давления и износа протектора шин';
                break;
            case 6:
                diaText = 'Диагностика, ремонт, заправка и чистка систем кондиционирования и отопления автомобиля';
                break;
            case 7:
                diaText = 'Замена жидкостей, свечей зажигания, фильтров и других расходных материалов. Регулировка светового потока фар, замена ламп освещения, щёток дворников, лобовых стёкол.';
                break;
        }
        $('.diagnostic__text').addClass('active');
        setTimeout(function(){$('.diagnostic__info img').addClass('active');}, 250);
        setTimeout(function(){$('.diagnostic__text').html(diaText);}, 500);
        setTimeout(function(){$('.diagnostic__text').removeClass('active');}, 500);
        setTimeout(function(){$('.diagnostic__info img').attr('src', 'img/diagnostic/' + (numberElem+1) + '.jpg');}, 650);
        setTimeout(function(){$('.diagnostic__info img').removeClass('active');}, 750);

    }
});
        //close&open form
$('.change-hf').click(function(){
    $('.hidden-form').toggleClass('hide');
});
function send_form(){
    $('.hidden-form').addClass('hide');
    $('form').trigger('reset');
    $('.success').removeClass('hide');
    setInterval(function(){
        $('.success').addClass('hide');
    }, 1500);
}
$('form').submit(function(e){
    e.preventDefault();
    var data_info = $(this).serialize();
    var phone = $(this).find('.input--tel');
    if(phone.val().length!=16){
        alert('Введите номер телефона полностью');
    } else {
        $.ajax({
            type: "POST", //Метод отправки
            url: "/forms/sendmail.php", //путь до php фаила отправителя
            data: data_info,
            success: function() {
                send_form();
            },
            error: function() {
                send_form();
            },
        });
    }
});
